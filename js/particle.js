let colorParticle = ["#fcc4fc", "#ffadad", "#bdb2ff"];

window.onload = function () {
  Particles.init({
    selector: ".background",
  });
};
const particles = Particles.init({
  selector: ".background",
  color: colorParticle,
  connectParticles: true,
  speed: 0.2,
  responsive: [
    {
      breakpoint: 1025,
      options: {
        color: colorParticle,
        maxParticles: 68,
        connectParticles: true,
      },
    },
    {
      breakpoint: 768,
      options: {
        color: colorParticle,
        maxParticles: 68,
        connectParticles: false,
      },
    },
  ],
});
