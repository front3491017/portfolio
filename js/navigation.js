checkbox = document.getElementById("navCheckBox");

checkbox.addEventListener("change", (e) => {
  let header = document.getElementById("header");
  if (e.target.checked) {
    header.classList.add("toggleNav");
  } else {
    header.classList.remove("toggleNav");
  }
});
